FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY /target/*.jar demo-server.jar
CMD ["java", "-jar", "/demo-server.jar"]


